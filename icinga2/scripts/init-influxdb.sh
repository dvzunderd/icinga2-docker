# Check if influxdb is online and icinga2 db exists
RET=1
while [[ RET -ne 0 ]]; do
   echo >&2 "=> Waiting for confirmation of influxdb service startup"
   sleep 5
   curl -i -XPOST http://influxdb:8086/query --data-urlencode "q=show DATABASEs" | grep -q '_internal' > /dev/null 2>&1
   RET=$?
done
echo >&2 "=> Influxdb service is running."

echo >&2 "=> Checking if icinga2 db exists"
curl -i -XPOST http://influxdb:8086/query --data-urlencode "q=show DATABASEs" | grep -q 'icinga2' > /dev/null 2>&1
RET=$?
if [[ RET -ne 0 ]]; then
  echo >&2 "==> does not exist. Creating"
  curl -i -XPOST http://influxdb:8086/query --data-urlencode "q=create DATABASE icinga2"
else
  echo >&2 "==> Exists nothing to do"
fi

# influxdb configuration
if [ ! -e "/etc/icinga2/features-enabled/influxdb.conf" ]; then
    echo >&2 "=> enabling influxdb"
    echo 'object InfluxdbWriter "influxdb" {' > /etc/icinga2/features-enabled/influxdb.conf
    echo '  host = "influxdb"' >> /etc/icinga2/features-enabled/influxdb.conf
    echo '  port = 8086' >> /etc/icinga2/features-enabled/influxdb.conf
    echo '  database = "icinga2"' >> /etc/icinga2/features-enabled/influxdb.conf
    echo '  flush_threshold = 1024' >> /etc/icinga2/features-enabled/influxdb.conf
    echo '  flush_interval = 10s' >> /etc/icinga2/features-enabled/influxdb.conf
    echo '  host_template = {' >> /etc/icinga2/features-enabled/influxdb.conf
    echo '    measurement = "$host.check_command$"' >> /etc/icinga2/features-enabled/influxdb.conf
    echo '    tags = {' >> /etc/icinga2/features-enabled/influxdb.conf
    echo '      hostname = "$host.name$"' >> /etc/icinga2/features-enabled/influxdb.conf
    echo '    }' >> /etc/icinga2/features-enabled/influxdb.conf
    echo '  }' >> /etc/icinga2/features-enabled/influxdb.conf
    echo '  service_template = {' >> /etc/icinga2/features-enabled/influxdb.conf
    echo '    measurement = "$service.check_command$"' >> /etc/icinga2/features-enabled/influxdb.conf
    echo '    tags = {' >> /etc/icinga2/features-enabled/influxdb.conf
    echo '      hostname = "$host.name$"' >> /etc/icinga2/features-enabled/influxdb.conf
    echo '      service = "$service.name$"' >> /etc/icinga2/features-enabled/influxdb.conf
    echo '    }' >> /etc/icinga2/features-enabled/influxdb.conf
    echo '  }' >> /etc/icinga2/features-enabled/influxdb.conf
    echo '}' >> /etc/icinga2/features-enabled/influxdb.conf
fi