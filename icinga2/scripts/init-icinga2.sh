echo >&2 "Init Icinga2"

# Copy configuration files
echo >&2 "==> Check if config available"

if  [ "$(ls -A /etc/icinga2)" ]; then
    echo >&2 "==> Found config doing nothing."
else
    echo >&2 "==> No config found. Copying default"
    cp -r /home/config/icinga2/* /etc/icinga2
fi

if [ "$(ls -A /run/icinga2)" ]; then
    echo "==> cmd exists do nothing"
else
    echo "==> cmd does not exist fixing rights"
    mkdir -p /run/icinga2/cmd
fi

# Make sure rights are set correctly for all folders:
chown -R nagios:root /etc/icinga2
chown -R nagios:nagios /var/spool/icinga2/perfdata
chown -R nagios:nagios /run/icinga2

chown -R nagios:root /var/log/icinga2
chown -R nagios:root /var/lib/icinga2
