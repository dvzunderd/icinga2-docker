# Waint until DB is running
RET=1
while [[ RET -ne 0 ]]; do
   echo >&2 "=> Waiting for confirmation of MySQL service startup"
   sleep 5
   mysql --host=${DB_HOST} --user=${MYSQL_ROOT_USER} --password=${MYSQL_ROOT_PASSWORD}  -e "status" > /dev/null 2>&1
   RET=$?
done
echo >&2 "=> MySQL service is running."

echo "==> Checking if ${ICINGA_DB} exists."
# Check if database already exists. If not then create one.
RESULT=`mysqlshow --host=${DB_HOST} --user=${MYSQL_ROOT_USER} --password=${MYSQL_ROOT_PASSWORD} ${ICINGA_DB} | grep -v Wildcard | grep -o ${ICINGA_DB}`
if [ "$RESULT" == "${ICINGA_DB}" ]; then
    echo >&2 "=> Database ${ICINGA_DB} exists."                                                                                                                                                          
    echo >&2 "=> Nothing to do." 
else
    echo >&2 "=> Database ${ICINGA_DB} does not exists"
    mysql --host=${DB_HOST} --user=${MYSQL_ROOT_USER} --password=${MYSQL_ROOT_PASSWORD} -e "CREATE DATABASE ${ICINGA_DB} CHARACTER SET 'utf8';"
    mysql --host=${DB_HOST} --user=${MYSQL_ROOT_USER} --password=${MYSQL_ROOT_PASSWORD} -e "CREATE USER ${ICINGA_DB_USER}@'%' IDENTIFIED WITH mysql_native_password BY '${ICINGA_DB_PASSWORD}';"
    mysql --host=${DB_HOST} --user=${MYSQL_ROOT_USER} --password=${MYSQL_ROOT_PASSWORD} -e "GRANT ALL ON ${ICINGA_DB}.* TO ${ICINGA_DB_USER}@'%';"
fi

echo "==> Checking if tables are created"
RESULT=`mysql --host=${DB_HOST} --user=${MYSQL_ROOT_USER} --password=${MYSQL_ROOT_PASSWORD} ${ICINGA_DB} -e "show tables;"`
if [[ ! -z "$RESULT" ]]; then
    echo >&2 "=> Tables already created."
    echo >&2 "=> Nothing to do."
else
    echo >&2 "=> Tables not yet created."
    echo >&2 "=> Creating." 

    # Import dtabase Schema
    mysql --host=${DB_HOST} --user=${ICINGA_DB_USER} --password=${ICINGA_DB_PASSWORD} ${ICINGA_DB} < /usr/share/icinga2-ido-mysql/schema/mysql.sql

    echo >&2 "=> Done"                                                                                                                                                                                                                                                                                                                                                  
fi

# IDO configuration
if [ ! -e "/etc/icinga2/features-enabled/ido-mysql.conf" ]; then
    echo >&2 "=> Changing ido_db config"
   
    sed -ri -e "s/^  user.*/  user = \"${ICINGA_DB_USER}\"/" /etc/icinga2/features-available/ido-mysql.conf
    sed -ri -e "s/^  password.*/  password = \"${ICINGA_DB_PASSWORD}\"/" /etc/icinga2/features-available/ido-mysql.conf
    sed -ri -e "s/^  database.*/  database = \"${ICINGA_DB}\"/" /etc/icinga2/features-available/ido-mysql.conf    
    sed -ri -e "s/^  host.*/  host = \"${DB_HOST}\"/" /etc/icinga2/features-available/ido-mysql.conf
 
    icinga2 feature enable ido-mysql

    echo >&2 "=> Done"  
fi