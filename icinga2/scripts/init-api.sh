# Echo API Configuration
echo "Checking if API is configured"
if [ `stat -c "%U" /var/lib/icinga2/api` != "nagios" ]; then
   echo "Fixing rights for dir /var/lib/icinga2/api"
   chown nagios:nagios -R /var/lib/icinga2/api
fi
if [ ! -e "/etc/icinga2/conf.d/api-users.conf" ]; then
   echo "configuring API"
   icinga2 api setup

   echo "object ApiUser \"${API_USER}\" {" >> /etc/icinga2/conf.d/api-users.conf
   echo "  password = \"${API_PASSWORD}\""  >> /etc/icinga2/conf.d/api-users.conf
   echo "  permissions = [ \"*\" ]"  >> /etc/icinga2/conf.d/api-users.conf
   echo "}"  >> /etc/icinga2/conf.d/api-users.conf
fi