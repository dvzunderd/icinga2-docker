#!/bin/bash

# Init icinga2
/scripts/init-icinga2.sh

# Configure database
/scripts/init-db.sh

# Configure Influxdb
/scripts/init-influxdb.sh

# Configure API
/scripts/init-api.sh

echo "Starting icinga2"
# exec /usr/sbin/icinga2 --no-stack-rlimit daemon -d -e /var/log/icinga2/icinga2.err
exec service icinga2 foreground
