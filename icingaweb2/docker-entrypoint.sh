#!/bin/bash
set -e

# Configure database
/scripts/init-db.sh

# Create config for icingaweb2 if necessary
/scripts/init-icingaweb2.sh

# Create config for directory if necessary
/scripts/init-director.sh

# Create config for Grafana if necessary
/scripts/init-grafana.sh

# create log file
touch /var/log/icingaweb2.log
chown root:icingaweb2 /var/log/icingaweb2.log
chmod 660 /var/log/icingaweb2.log

# Set permissions for the etc folder
chown root:icingaweb2 -R /etc/icingaweb2/
chmod -R 770 /etc/icingaweb2

# Configure time-zone
sed -ri -e "s|^;date.timezone.*|date.timezone = $TIME_ZONE|" /etc/php/7.3/fpm/php.ini

# Start director
/usr/bin/icingacli director daemon run &

# exec  /usr/sbin/php-fpm7.3 --nodaemonize --fpm-config /etc/php/7.3/fpm/php-fpm.conf
rm -f /var/run/apache2/apache2.pid
exec apache2 -DFOREGROUND "$@"