#!/bin/bash

#
# Enable modules
# 

icingacli module enable incubator
icingacli module enable director

#
# Create database
#

# Director database
RESULT=`mysqlshow --host=db --user=${MYSQL_ROOT_USER} --password=${MYSQL_ROOT_PASSWORD} | grep -o ${DIRECTOR_DB} || echo ""`
if [ ! "${RESULT}" == "${DIRECTOR_DB}" ]; then
   echo "> Creating database: ${DIRECTOR_DB}"
   mysql --host=${DB_HOST} --user=${MYSQL_ROOT_USER} --password=${MYSQL_ROOT_PASSWORD} -e "CREATE DATABASE ${DIRECTOR_DB} CHARACTER SET 'utf8';"
   mysql --host=${DB_HOST} --user=${MYSQL_ROOT_USER} --password=${MYSQL_ROOT_PASSWORD} -e "CREATE USER ${DIRECTOR_DB_USER}@'%' IDENTIFIED WITH mysql_native_password BY '${DIRECTOR_DB_PASSWORD}';"
   mysql --host=${DB_HOST} --user=${MYSQL_ROOT_USER} --password=${MYSQL_ROOT_PASSWORD} -e "GRANT ALL ON ${DIRECTOR_DB}.* TO ${DIRECTOR_DB_USER}@'%';"
else
   echo "> Skipping database creation: ${DIRECTOR_DB}"
fi

#
# Configuration
#

mkdir -p /etc/icingaweb2/modules/director

# config.ini
if [ ! -e "/etc/icingaweb2/modules/director/config.ini" ]; then
   # config.ini
   echo "[db]" >> /etc/icingaweb2/modules/director/config.ini
   echo "resource = \"Director_DB\"" >> /etc/icingaweb2/modules/director/config.ini

   # icingaweb2 resources.ini
   echo "\n" >> /etc/icingaweb2/resources.ini
   echo "[Director_DB]" >> /etc/icingaweb2/resources.ini
   echo "type = \"db\"" >> /etc/icingaweb2/resources.ini
   echo "db = \"mysql\"" >> /etc/icingaweb2/resources.ini
   echo "host = \"${DB_HOST}\"" >> /etc/icingaweb2/resources.ini
   echo "dbname = \"${DIRECTOR_DB}\"" >> /etc/icingaweb2/resources.ini
   echo "username = \"${DIRECTOR_DB_USER}\"" >> /etc/icingaweb2/resources.ini
   echo "password = \"${DIRECTOR_DB_PASSWORD}\"" >> /etc/icingaweb2/resources.ini
   echo "charset = \"utf8\"" >> /etc/icingaweb2/resources.ini

   # Kickstarter config\
   echo "[config]"  >> /etc/icingaweb2/modules/director/kickstart.ini
   echo "endpoint = icinga2-master" >> /etc/icingaweb2/modules/director/kickstart.ini
   echo "; host = 127.0.0.1" >> /etc/icingaweb2/modules/director/kickstart.ini
   echo "; port = 5665" >> /etc/icingaweb2/modules/director/kickstart.ini
   echo "username = ${API_USER}" >> /etc/icingaweb2/modules/director/kickstart.ini
   echo "password = ${API_PASSWORD}" >> /etc/icingaweb2/modules/director/kickstart.ini
fi 

#
# Initialiseer de database
#
if (icingacli director migration pending) ; then
   echo "Database needs initialization"
   icingacli director migration run
fi

# 
# Run the kickstart if needed.
#
if (icingacli director kickstart required) ; then
  icingacli director kickstart run
fi
