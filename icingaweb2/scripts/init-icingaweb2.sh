#!/bin/bash

#
# Enable modules
#

icingacli module enable monitoring
icingacli module enable doc

#
# /etc/icingaweb2 configuration
#

# config.ini
if [ ! -e "/etc/icingaweb2/config.ini" ]; then
   echo "[global]" >> /etc/icingaweb2/config.ini
   echo "show_stacktraces = \"0\"" >> /etc/icingaweb2/config.ini
   echo "show_application_state_messages = \"0\"" >> /etc/icingaweb2/config.ini
   echo "config_backend = \"ini\"" >> /etc/icingaweb2/config.ini

   echo "[logging]" >> /etc/icingaweb2/config.ini
   echo "log = \"file\"" >> /etc/icingaweb2/config.ini
   echo "level = \"INFO\"" >> /etc/icingaweb2/config.ini
   echo "file = \"/var/log/icingaweb2.log\"" >> /etc/icingaweb2/config.ini
#    echo "application = \"icingaweb2\"" >> /etc/icingaweb2/config.ini
#    echo "facility = \"user\"" >> /etc/icingaweb2/config.ini
fi

# resources.ini
if [ ! -e "/etc/icingaweb2/resources.ini" ]; then
   echo "[icingaweb_db]" >> /etc/icingaweb2/resources.ini
   echo "type = \"db\"" >> /etc/icingaweb2/resources.ini
   echo "db = "mysql"" >> /etc/icingaweb2/resources.ini
   echo "host = \"${DB_HOST}\"" >> /etc/icingaweb2/resources.ini
   echo "port = \"\"" >> /etc/icingaweb2/resources.ini
   echo "dbname = \"${ICINGAWEB2_DB}\"" >> /etc/icingaweb2/resources.ini
   echo "username = \"${ICINGAWEB2_DB_USER}\"" >> /etc/icingaweb2/resources.ini
   echo "password = \"${ICINGAWEB2_DB_PASSWORD}\"" >> /etc/icingaweb2/resources.ini
   echo "charset = \"utf8\"" >> /etc/icingaweb2/resources.ini
   echo "use_ssl = \"0\"" >> /etc/icingaweb2/resources.ini
   echo "\n" >> /etc/icingaweb2/resources.ini

   echo "[icinga_ido]" >> /etc/icingaweb2/resources.ini
   echo "type = \"db\"" >> /etc/icingaweb2/resources.ini
   echo "db = \"mysql\"" >> /etc/icingaweb2/resources.ini
   echo "host = \"${DB_HOST}\"" >> /etc/icingaweb2/resources.ini
   echo "port = \"\"" >> /etc/icingaweb2/resources.ini
   echo "dbname = \"${ICINGA_DB}\"" >> /etc/icingaweb2/resources.ini
   echo "username = \"${ICINGA_DB_USER}\"" >> /etc/icingaweb2/resources.ini
   echo "password = \"${ICINGA_DB_PASSWORD}\"" >> /etc/icingaweb2/resources.ini
   echo "charset = \"utf8\"" >> /etc/icingaweb2/resources.ini
   echo "use_ssl = \"0\"" >> /etc/icingaweb2/resources.ini
fi

# authentication.ini
if [ ! -e "/etc/icingaweb2/authentication.ini" ]; then
   echo "[icingaweb2]" >> /etc/icingaweb2/authentication.ini
   echo "backend = \"db\"" >> /etc/icingaweb2/authentication.ini
   echo "resource = \"icingaweb_db\"" >> /etc/icingaweb2/authentication.ini
fi

# groups.ini
if [ ! -e "/etc/icingaweb2/groups.ini" ]; then
   echo "[icingaweb2]" >> /etc/icingaweb2/groups.ini
   echo "backend = \"db\"" >> /etc/icingaweb2/groups.ini
   echo "resource = \"icingaweb_db\"" >> /etc/icingaweb2/groups.ini
fi

# roles.ini
if [ ! -e "/etc/icingaweb2/roles.ini" ]; then
   echo "[Administrators]" >> /etc/icingaweb2/roles.ini
   echo "users = \"admin\"" >> /etc/icingaweb2/roles.ini
   echo "permissions = \"*\"" >> /etc/icingaweb2/roles.ini
   echo "groups = \"Administrators\"" >> /etc/icingaweb2/roles.ini
fi

#
# /etc/icingaweb2/modules/monitoring configuration
#

mkdir -p /etc/icingaweb2/modules/monitoring

# config.ini
if [ ! -e "/etc/icingaweb2/modules/monitoring/config.ini" ]; then
   echo "[security]" >> /etc/icingaweb2/modules/monitoring/config.ini
   echo "protected_customvars = \"*pw*,*pass*,community\"" >> /etc/icingaweb2/modules/monitoring/config.ini
fi

# backends.ini
if [ ! -e "/etc/icingaweb2/modules/monitoring/backends.ini" ]; then
   echo "[icinga]" >> /etc/icingaweb2/modules/monitoring/backends.ini
   echo "type = \"ido\"" >> /etc/icingaweb2/modules/monitoring/backends.ini
   echo "resource = \"icinga_ido\"" >> /etc/icingaweb2/modules/monitoring/backends.ini
fi

# commandtransports.ini
if [ ! -e "/etc/icingaweb2/modules/monitoring/commandtransports.ini" ]; then
   echo "[icinga2]" >> /etc/icingaweb2/modules/monitoring/commandtransports.ini
   echo "transport = \"api\"" >> /etc/icingaweb2/modules/monitoring/commandtransports.ini
   echo "host = \"icinga2-master\"" >> /etc/icingaweb2/modules/monitoring/commandtransports.ini
   echo "port = \"5665\"" >> /etc/icingaweb2/modules/monitoring/commandtransports.ini
   echo "username = \"${API_USER}\"" >> /etc/icingaweb2/modules/monitoring/commandtransports.ini
   echo "password = \"${API_PASSWORD}\"" >> /etc/icingaweb2/modules/monitoring/commandtransports.ini
fi
