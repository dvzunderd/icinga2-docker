#
# Waint until DB is running
#

RET=1
while [[ RET -ne 0 ]]; do
   echo >&2 "=> Waiting for confirmation of MySQL service startup"
   sleep 5
   mysql --host=${DB_HOST} --user=${MYSQL_ROOT_USER} --password=${MYSQL_ROOT_PASSWORD}  -e "status" > /dev/null 2>&1
   RET=$?
done
echo >&2 "=> MySQL service is running."

#
# initialize database
# 

# Icingaweb2 database
RESULT=`mysqlshow --host=${DB_HOST} --user=${MYSQL_ROOT_USER} --password=${MYSQL_ROOT_PASSWORD} | grep -o ${ICINGAWEB2_DB} || echo ""`
if [ ! "${RESULT}" == "${ICINGAWEB2_DB}" ]; then
   echo "> Creating database: ${ICINGAWEB2_DB}"
   # Create database
   mysql --host=${DB_HOST} --user=${MYSQL_ROOT_USER} --password=${MYSQL_ROOT_PASSWORD} -e "CREATE DATABASE ${ICINGAWEB2_DB} CHARACTER SET 'utf8';"
   mysql --host=${DB_HOST} --user=${MYSQL_ROOT_USER} --password=${MYSQL_ROOT_PASSWORD} -e "CREATE USER ${ICINGAWEB2_DB_USER}@'%' IDENTIFIED WITH mysql_native_password BY '${ICINGAWEB2_DB_PASSWORD}';"
   mysql --host=${DB_HOST} --user=${MYSQL_ROOT_USER} --password=${MYSQL_ROOT_PASSWORD} -e "GRANT ALL ON ${ICINGAWEB2_DB}.* TO ${ICINGAWEB2_DB_USER}@'%';"

   # Import tables
   echo "> Importing tables: ${ICINGAWEB2_DB}"
   mysql --host=${DB_HOST} --user=${ICINGAWEB2_DB_USER} --password=${ICINGAWEB2_DB_PASSWORD} ${ICINGAWEB2_DB} < /usr/share/icingaweb2/etc/schema/mysql.schema.sql

   # Create admin user for icingaweb2
   echo "> Creating admin user: ${ICINGAWEB2_USER}"
   # PASSWD=$(php -r "echo password_hash(\"${ICINGAWEB2_PASSWORD}\", PASSWORD_DEFAULT);")
   PASSWD=$(echo ${ICINGAWEB2_PASSWORD} | openssl passwd -1 -stdin)
   mysql --host=${DB_HOST} --user=${ICINGAWEB2_DB_USER} --password=${ICINGAWEB2_DB_PASSWORD} ${ICINGAWEB2_DB} -e "INSERT INTO icingaweb_user (name, active, password_hash) VALUES ('${ICINGAWEB2_USER}', 1, '${PASSWD}');"
else
   echo "> Skipping database creation: ${ICINGAWEB2_DB}"
fi
