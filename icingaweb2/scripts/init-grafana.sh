#
# enable modules
#

icingacli module enable 

#
# Configuration
#

mkdir -p /etc/icingaweb2/modules/grafana

# config.ini
if [ ! -e "/etc/icingaweb2/modules/grafana/config.ini" ]; then
   # config.ini
   echo "[grafana]" >> /etc/icingaweb2/modules/grafana/config.ini
   echo "host = \"grafana:3000\"" >> /etc/icingaweb2/modules/grafana/config.ini
   echo "protocol = \"http\"" >> /etc/icingaweb2/modules/grafana/config.ini
   echo "defaultdashboard = \"icinga2-default\"" >> /etc/icingaweb2/modules/grafana/config.ini
   echo "shadows = \"0\"" >> /etc/icingaweb2/modules/grafana/config.ini
   echo "defaultdashboardstore = \"db\"" >> /etc/icingaweb2/modules/grafana/config.ini
   echo "theme = \"light\"" >> /etc/icingaweb2/modules/grafana/config.ini
   echo "datasource = \"influxdb\"" >> /etc/icingaweb2/modules/grafana/config.ini
   echo "accessmode = \"direct\"" >> /etc/icingaweb2/modules/grafana/config.ini
   echo "directrefresh = \"no\"" >> /etc/icingaweb2/modules/grafana/config.ini
   echo "height = \"280\"" >> /etc/icingaweb2/modules/grafana/config.ini
   echo "width = \"640\"" >> /etc/icingaweb2/modules/grafana/config.ini
   echo "enableLink = \"no\"" >> /etc/icingaweb2/modules/grafana/config.ini
fi